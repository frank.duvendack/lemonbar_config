#Define common colors
module Lemonbar_colors
  @accent = "#ff0000"

  #Load primary accent color from a file called 'color' located in a directory specified by
  #an environment variable called 'LEMONBAR_CONFIG_DIR'.
  def getAccent()
    File.open("#{ENV["LEMONBAR_CONFIG_DIR"]}/color", "r") {|f| @accent = f.gets.strip}
    @accent
  end

  $FG_COLOR = "#FFFFFF"
  $FG2_COLOR = "#888888"
end

#Define methods for formatting strings for Lemonbar
module Lemonbar_format
  #center align a string
  def alignCenter(str)
    "%{c}#{str}"
  end

  #right align a string
  def alignRight(str)
    "%{r}#{str}"
  end

  #left align a string
  def alignLeft(str)
    "%{l}#{str}"
  end

  #set a string's background color
  def background(color, str)
    "%{B#{color}}#{str}%{B-}"
  end

  #set a string's foreground color
  def foreground(color, str)
    "%{F#{color}}#{str}%{F-}"
  end
end

#Meta module that includes Lemonbar_colors and Lemonbar_format
module Lemonbar_base
  include Lemonbar_colors
  include Lemonbar_format

  #formatted string to be displayed by Lemonbar shall be stored in @value
  #@value shall be set by the 'render' method of whatever class includes Lemonbar_base
  attr_reader :value
  def initialize
    objinit()
    @value = render()
  end
end
