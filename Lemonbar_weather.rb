require_relative 'Lemonbar_base.rb'
require 'json'
require 'net/http'

class Lemonbar_weather_data

  attr_reader :currentTemp

  def initialize
    @@OPENWEATHER_API_KEY = ENV["OPENWEATHER_API_KEY"]
    @@OPENWEATHER_ZIP_CODE = ENV["OPENWEATHER_ZIP_CODE"]

    raise "Environment variable \"OPENWEATHER_API_KEY\" not set"  if @@OPENWEATHER_API_KEY == nil 
    raise "Environment variable \"OPENWEATHER_ZIP_CODE\" not set" if @@OPENWEATHER_ZIP_CODE == nil 

    @currentTemp = 0
    update()
  end

  def update
    begin
      json = Net::HTTP.get(URI("http://api.openweathermap.org/data/2.5/weather?zip=#{@@OPENWEATHER_ZIP_CODE}&appid=#{@@OPENWEATHER_API_KEY}&units=metric"))
      weather_data = JSON.parse(json)
      @currentTemp = weather_data["main"]["temp"]
    rescue
      #do nothing; hold previous temperature
    end
  end
end

class Lemonbar_weather
  include Lemonbar_base

  def objinit 
    @weatherdata = Lemonbar_weather_data.new
  end

  def render
    @weatherdata.update()
    @value = foreground(getAccent(), " %0.2f\u00B0C " % [@weatherdata.currentTemp])
  end
end
