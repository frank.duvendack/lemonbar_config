require_relative 'Lemonbar_base.rb'

#display a 'あ' if the current ibus ime is mozc-jp
#display nothing if another ime is selected
class Lemonbar_ime
  include Lemonbar_base

  def objinit
    #do nothing
  end

  def render
    imeSym = getIme()
    @value = foreground(getAccent(), " #{imeSym} ")
  end

  private

  #get the symbol to display (either 'あ' or ' ') depending on which ime is enabled
  def getIme
    if %x(ibus engine).strip == "mozc-jp"
      "あ" 
    else
      " "
    end
  end
end
