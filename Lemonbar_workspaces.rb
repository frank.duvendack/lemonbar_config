require_relative 'Lemonbar_base'

#display workspace indicators for BSPWM
class Lemonbar_workspaces
  include Lemonbar_base

  def objinit
    #do nothing
  end

  def render
    #get the currently selected desktop
    activedesktop = %x(bspc query -D -d focused --names).strip

    #process all desktops on all monitors
    @value = %x(bspc query -D --names).split("\n").map.with_index do |desktop, i|
      #get nodes on desktop
      nodes = %x(bspc query -d #{desktop} -N).split("\n")

      if desktop == activedesktop
        #display active desktop in white with accent color background
        newdesktop = background(getAccent, foreground($FG_COLOR, " #{desktop} ")) 
      else
        if nodes.length > 0
          #display non-empty, non-active desktops in accent color
          newdesktop = foreground(getAccent, " #{desktop} ")
        else
          #display empty, non-active desktops in gray
          newdesktop = foreground($FG2_COLOR, " #{desktop} ") 
        end
      end

      #the first 9 desktops are on monitor 1 and the rest will be on monitor 2
      #separate the indicators with a '|' character
      if i == 9
        newdesktop = "| #{newdesktop}"
      end

      desktop = newdesktop
    end.join(" ")
  end
end
