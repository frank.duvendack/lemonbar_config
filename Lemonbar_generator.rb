require_relative 'Lemonbar_clock'
require_relative 'Lemonbar_workspaces'
require_relative 'Lemonbar_weather'

#main generator class responsible for generating a string to be displayed by Lemonbar.
class Lemonbar_generator
  include Lemonbar_base

  #all blocks shall be initialized here
  def initialize
    #cycle counter used for triggering certain blocks to refresh at certain time
    #intevals
    @cycleCounter = 0

    #initialize blocks
    #all blocks shall include the 'Lemonbar_base' module and provide a render method
    #for updating their string
    @clock = Lemonbar_clock.new()
    @workspaces = Lemonbar_workspaces.new()
    @weather = Lemonbar_weather.new()

    @prevtime = Time.now

    @prevAccent = getAccent()
  end

  #start an infinite loop that constantly outputs a string to be displayed by Lemonbar
  def beginloop
    loop do
      #get strings from all blocks and combine into a single string
      leftalgn = @workspaces.value

      #rightalgn = @ime.value
      rightalgn = @weather.value
      rightalgn += @clock.value

      lemonbarStr = leftalgn + alignRight(rightalgn)

      #call 'render' method on all blocks (assuming @cycleCounter is at an acceptable
      #interval for the block)
      updateBlocks()

      #display rendered string
      puts lemonbarStr

      #refresh string at a fixed rate
      sleep $REFRESH_RATE
    end
  end


  private
  
  #how often to refresh Lemonbar in seconds
  $REFRESH_RATE = 0.1

  #how many refreshes happen in 1 second
  $REFRESHES_PER_SECOND = 1 / $REFRESH_RATE

  #re-render all blocks
  def updateBlocks

    currenttime = Time.now

    #force an update if the difference between now and prevtime is greater than 5 seconds.
    #this will happen if program execution is paused (such as when the pc goes to sleep).
    forceUpdate = (currenttime - @prevtime).to_i > 5

    #wait 10 seconds to allow the network interface to start back up
    sleep 10 if forceUpdate

    #force an update if the accent color changes
    newAccent = getAccent()
    forceUpdate = forceUpdate || @prevAccent != newAccent
    @prevAccent = newAccent

    #update blocks every cycle
    @workspaces.render

    #update blocks every 10 seconds
    if @cycleCounter % ($REFRESHES_PER_SECOND * 10) == 0 || forceUpdate
      @clock.render
    end

    #update blocks every 30 minutes
    if @cycleCounter % ($REFRESHES_PER_SECOND * 60 * 30) == 0 || forceUpdate
      @weather.render
      @cycleCounter = 0
    end

    @cycleCounter += 1

    @prevtime = Time.now
  end
end
